use failure::{Error, ResultExt};
use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    pub use_76: bool,
    pub smtp: Smtp,
    pub calibration: Calibrations,
    pub alerts: Alerts,
    pub webui: Webui,
    pub site_name: String,
    pub fs: Fs,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Smtp {
    pub server_domain: String,
    pub user: String,
    pub pass: String,
    pub from_addr: String,
    pub client_hostname: String,
    pub reports_schedule: String,
    // Email addresses separated by commas.  Spaces not allowed.
    pub subscribers: String,
}

impl Smtp {
    pub fn subscribers(&self) -> Vec<String> {
        self.subscribers.split(',').map(|s| s.to_string()).collect()
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct Calibrations {
    pub temperature: Calibration,
    pub humidity: Calibration,
    pub pressure: Calibration,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Calibration {
    pub offset: f64,
    pub factor: f64,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Alerts {
    pub temperature: Option<Alert>,
    // pub humidity: Option<Alert>,
    // pub pressure: Option<Alert>,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Alert {
    pub trigger: f64,
    pub clear: f64,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Webui {
    pub listen_port: u16,
    pub listen_ip: String,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Fs {
    pub tmp: String,
    pub db: String,
}

impl Config {
    pub fn load() -> Result<Config, Error> {
        let mut settings = config::Config::default();

        settings
            .merge(config::File::with_name("default_settings"))
            .context("Error loading default_settings file")?;
        settings
            .merge(config::File::with_name("test_settings").required(false))
            .context("Error loading test_settings file")?;
        settings
            .merge(config::Environment::new().separator("__"))
            .context("Error merging environment vars into config")?;

        Ok(settings.try_into()?)
    }
}
