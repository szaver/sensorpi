#![warn(clippy::all)]

mod alert;
mod config;
mod email;
// mod plot;
mod reader;
mod reading;
mod report;
mod scheduler;
mod sensor;
mod server;
mod storage;

use crate::config::Config;
use crate::reader::reader;
use crate::scheduler::{join_threads, run_thread};
use crate::server::server;
use crate::storage::Storage;
use failure::{Error, ResultExt};
use parking_lot::Mutex;
use std::sync::Arc;
use std::time::Duration as StdDuration;

#[derive(Debug, Clone)]
pub struct SharedData {
    pub storage: Arc<Mutex<Storage>>,
    pub read_interval: StdDuration,
    pub config: Config,
}

fn main() -> Result<(), Error> {
    // how often to read the sensor
    let read_interval = StdDuration::from_millis(1000);

    // load user-defined config from the environment
    let config = Config::load().context("Error loading configuration")?;

    // logging
    env_logger::init();

    // storage
    let temperature_storage = Storage::new(config.fs.db.clone(), "temperature".into());

    // shared data container
    let data = SharedData {
        storage: Arc::new(Mutex::new(temperature_storage)),
        read_interval,
        config,
    };

    join_threads(vec![
        run_thread("reader", &data, reader)?,
        run_thread("server", &data, server)?,
    ]);

    Ok(())
}
