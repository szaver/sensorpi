pub mod bme280;

use crate::reading::Reading;
use failure::Error;

pub trait Sensor {
    // sensors may read multiple things at once, so we return a vec
    fn read(&mut self) -> Result<Vec<Reading>, Error>;
}
