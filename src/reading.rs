use crate::config::Calibration;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
// use stats::{MinMax, OnlineStats};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub enum Quantity {
    Temperature,
    Humidity,
    Pressure,
}
use Quantity::*;

impl Quantity {
    pub fn name_title(&self) -> String {
        match self {
            Temperature => "Temperatura".into(),
            Humidity => "Humedad".into(),
            Pressure => "Pression".into(),
        }
    }

    pub fn unit(&self) -> String {
        match self {
            Temperature => "C".into(),
            Humidity => "%".into(),
            Pressure => "hPa".into(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Reading {
    pub sensor: String,
    pub quantity: Quantity,
    pub value: f64,
    pub timestamp: DateTime<Utc>,
}

impl Reading {
    pub fn now(
        sensor: impl Into<String>,
        quantity: impl Into<Quantity>,
        value: impl Into<f64>,
    ) -> Self {
        Self {
            sensor: sensor.into(),
            quantity: quantity.into(),
            value: value.into(),
            timestamp: Utc::now(),
        }
    }
    pub fn calibrate(&mut self, calibration: &Calibration) -> Self {
        Self {
            value: self.value * calibration.factor + calibration.offset,
            ..self.clone()
        }
    }
}

// #[derive(Serialize, Deserialize, Debug, Clone)]
// pub struct AggregateReading {
//     pub start_timestamp: chrono::DateTime<Utc>,
//     pub end_timestamp: chrono::DateTime<Utc>,
//     pub mean: f64,
//     pub min: f64,
//     pub max: f64,
//     pub stddev: f64,
//     pub quantity: Quantity,
// }

// /// Pass through the contents of the option, or immediately return None
// macro_rules! return_none {
//     ($expr:expr) => {
//         match $expr {
//             Some(v) => v,
//             None => return None,
//         };
//     };
// }

// impl AggregateReading {
//     pub fn from_readings(quantity: Quantity, readings: &[Reading]) -> Option<Self> {
//         let mut min_max = MinMax::new();
//         let mut stats = OnlineStats::new();
//         let mut start_timestamp = None;
//         let mut end_timestamp = None;

//         for (i, reading) in readings
//             .iter()
//             .filter(|r| r.quantity == quantity)
//             .enumerate()
//         {
//             if i == 0 {
//                 start_timestamp = Some(reading.timestamp);
//             }
//             end_timestamp = Some(reading.timestamp);

//             min_max.add(reading.value);
//             stats.add(reading.value);
//         }

//         let start_timestamp = return_none!(start_timestamp);
//         let end_timestamp = return_none!(end_timestamp);
//         let min = *return_none!(min_max.min());
//         let max = *return_none!(min_max.max());

//         Some(AggregateReading {
//             start_timestamp,
//             end_timestamp,
//             mean: stats.mean(),
//             stddev: stats.stddev(),
//             min,
//             max,
//             quantity,
//         })
//     }
// }
