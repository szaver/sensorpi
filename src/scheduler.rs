use failure::Error;
use std::thread;
use std::thread::JoinHandle;

#[allow(dead_code)]
pub fn run_thread<F, T>(
    name: impl Into<String>,
    data: &T,
    entrypoint: F,
) -> Result<JoinHandle<()>, Error>
where
    F: Fn(T) -> Result<(), Error>,
    F: Send + Copy + 'static,
    T: Clone + Send + Sync + 'static,
{
    let data = data.clone();

    let builder = thread::Builder::new().name(name.into());

    let handle = builder.spawn(move || {
        if let Err(e) = entrypoint(data) {
            panic!("{:?}", e);
        }
    })?;

    Ok(handle)
}

pub fn join_threads<T>(handles: Vec<JoinHandle<T>>) {
    for handle in handles {
        handle.join().expect("A child thread panicked.");
    }
}
