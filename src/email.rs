use failure::Error;
use lettre::smtp::authentication::{Credentials, Mechanism};
use lettre::smtp::error::Error as SmtpError;
use lettre::smtp::extension::ClientId;
use lettre::smtp::ConnectionReuseParameters::ReuseUnlimited;
use lettre::{SmtpTransport, Transport};
use lettre_email::EmailBuilder;
use std::cell::RefCell;
use std::env;

pub struct SmtpClient {
    mailer: Option<RefCell<SmtpTransport>>,
    from_addr: String,
}

impl SmtpClient {
    pub fn new(
        server_domain: &str,
        from_addr: &str,
        user: &str,
        pass: &str,
    ) -> Result<Self, SmtpError> {
        let mailer = match env::var_os("NO_EMAIL") {
            Some(_) => None,
            None => Some(RefCell::new(
                lettre::SmtpClient::new_simple(server_domain)?
                    // Set the name sent during EHLO/HELO, default is `localhost`
                    .hello_name(ClientId::Domain("sensorpi.whiting.io".to_string()))
                    // Add credentials for authentication
                    .credentials(Credentials::new(user.to_string(), pass.to_string()))
                    // Enable SMTPUTF8 if the server supports it
                    .smtp_utf8(true)
                    // Configure expected authentication mechanism
                    .authentication_mechanism(Mechanism::Plain)
                    // allow reusing the conection
                    .connection_reuse(ReuseUnlimited)
                    .transport(),
            )),
        };

        Ok(Self {
            mailer: mailer,
            from_addr: from_addr.to_string(),
        })
    }

    pub fn send(&self, email: impl Into<lettre::SendableEmail>) -> Result<(), SmtpError> {
        if let Some(mailer) = &self.mailer {
            mailer.borrow_mut().send(email.into())?;
        }

        Ok(())
    }

    pub fn html_builder(&self, to: &str, subject: &str, text: &str) -> lettre_email::EmailBuilder {
        EmailBuilder::new()
            .to(to)
            .from(self.from_addr.clone())
            .subject(subject)
            .html(text)
    }

    pub fn send_plain(&self, to: &str, subject: &str, text: &str) -> Result<(), Error> {
        let email = EmailBuilder::new()
            .to(to)
            .from(self.from_addr.clone())
            .subject(subject)
            .text(text)
            .build()?;

        self.send(email)?;

        Ok(())
    }
}

// #[derive(Debug, Fail)]
// pub enum BuildError {
//     #[fail(display = "Error initializing SmtpTransport::simple_builder")]
//     BuilderInitError(lettre::smtp::error::Error),
// }
