use crate::reading::Reading;
use failure::{err_msg, Error, Fail, ResultExt};
use log::{Level::*, *};
use std::process::Command;
use std::{io, io::prelude::*};

#[derive(Debug)]
pub struct Storage {
    db_path: String,
    variable_name: String,
    last_entry: Option<Reading>,
}

impl Storage {
    // create new instance of self with configuration
    pub fn new(db_path: String, variable_name: String) -> Self {
        Self {
            db_path,
            last_entry: None,
            variable_name,
        }
    }

    pub fn add(&mut self, reading: Reading) -> Result<(), Error> {
        // record last reading in memory regardless of whether it was successfully saved
        self.last_entry = Some(reading.clone());

        let timestamp = reading.timestamp.format("%s");
        let update_expr = format!(
            "{timestamp}:{value}",
            timestamp = timestamp,
            value = reading.value
        );

        // Build the command
        let mut command = Command::new("rrdtool");
        command.arg("update");
        command.arg(&self.db_path);
        command.arg(update_expr);

        // execute the command
        info!("$ {:?}", command);
        let status = command.status().context("Error running rrdtool update")?;

        if status.success() {
            Ok(())
        } else {
            Err(err_msg("rrdtool returned a failing exit code"))
        }
    }

    pub fn last_entry(&self) -> Option<&Reading> {
        self.last_entry.as_ref()
    }

    pub fn graph(&self, end: &str) -> Result<Vec<u8>, Error> {
        // Build the command
        let mut command = Command::new("rrdtool");
        command.arg("graph");
        command.arg("-"); // send bytes to stdout
        command.args(&["--end", "now"]);
        command.args(&["--start", end]);
        command.args(&["--vertical-label", "grados C"]);
        command.arg(format!(
            "DEF:graphtemp={file}:{var}:AVERAGE",
            file = self.db_path,
            var = self.variable_name
        ));
        command.arg("LINE2:graphtemp#FF0000");

        // run the command
        info!("$ {:?}", command);
        let output = command
            .output()
            .map_err(|e| Error::from(e.context("creating a graph with RRDtool")))?;

        debug!("status: {}", output.status);
        if log_enabled!(Debug) {
            // io::stdout()
            //     .write_all(&output.stdout.clone())
            //     .context("writing stdout")?;
            io::stderr()
                .write_all(&output.stderr)
                .context("writing stderr")?;
        }

        if output.status.success() {
            Ok(output.stdout)
        } else {
            Err(err_msg(format!(
                "Rrdtool exited with code {}",
                output
                    .status
                    .code()
                    .map(|code| code.to_string())
                    .unwrap_or_else(|| String::from("unknown"))
            )))
        }
    }
}
