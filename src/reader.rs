use crate::alert::Alert;
use crate::email::SmtpClient;
use crate::reading::Quantity::*;
use crate::report::send_report;
use crate::sensor::bme280::Bme280;
use crate::sensor::Sensor;
use crate::SharedData;
use failure::{err_msg, Error, ResultExt};
use job_scheduler::{Job, JobScheduler};
use log::{debug, error, info};
use std::process::Command;
use std::sync::Arc;
use std::thread;

macro_rules! try_or_continue {
    ($code:expr, $msg:expr) => {
        match $code {
            Ok(val) => val,
            Err(e) => {
                error!("{} {}", $msg, e);
                continue;
            }
        }
    };
}

pub fn reader(data: SharedData) -> Result<(), Error> {
    let config = data.config;
    let temperature_storage = data.storage;
    let read_interval = data.read_interval;

    // sensor reader
    let mut bme280 = Bme280::new(
        "bme280",
        &config.calibration,
        if config.use_76 {
            Command::new("/bme280-76")
        } else {
            Command::new("/bme280-77")
        },
    );

    // email client
    let smtp_client = SmtpClient::new(
        &config.smtp.server_domain,
        &config.smtp.from_addr,
        &config.smtp.user,
        &config.smtp.pass,
    )
    .context("Error creating SMTP Client")?;
    let email = Arc::new(smtp_client);

    // alerts
    let subscribers: Vec<String> = config.smtp.subscribers();

    let mut temperature_alert = Alert::new(
        email.clone(),
        &config.alerts.temperature,
        Temperature,
        &config.site_name,
        subscribers.clone(),
    );

    // Send a report every day
    let mut scheduler = JobScheduler::new();
    scheduler.add(Job::new(
        config.smtp.reports_schedule.parse().unwrap(),
        || {
            // send email report
            info!("Sending email report");
            send_report(&config, temperature_storage.clone(), &email)
                .expect("Error sending report");
            info!("Done sending email report");
        },
    ));

    let mut is_first = true;
    loop {
        if !is_first {
            debug!(
                "putting the current thread to sleep for {:?}",
                read_interval
            );

            // Sleep a while before continuing
            thread::sleep(read_interval);

            debug!("Woke up from sleep");
        }
        is_first = false;

        // run scheduled tasks
        debug!("tick scheduler");
        scheduler.tick();

        // Should return a Vec with three items inside.
        // Temperature reading, humidity reading, and pressure reading.
        debug!("read bme280");
        let readings = try_or_continue!(bme280.read(), "Error reading bme280 sensor!");
        debug!("finish reading bme280");
        for reading in &readings {
            info!("{:?}", reading);
        }

        // Save the readings
        debug!("Find temperature reading");
        let temperature_reading = readings
            .iter()
            .find(|reading| reading.quantity == Temperature)
            .ok_or_else(|| err_msg("Missing temperature reading"))?;
        debug!("Save reading to storage");
        temperature_storage
            .lock()
            .add(temperature_reading.clone())?;

        // process alerts
        debug!("Process alerts");
        temperature_alert.process_batch(readings.clone())?;

        debug!("Go back to start");
    }
}
