use crate::SharedData;
use failure::{err_msg, Error};
use prometheus::{Encoder, Gauge, Registry, TextEncoder};
use std::net::IpAddr;
use warp::{
    http::{header::*, Response, StatusCode},
    path, Filter, Rejection, Reply,
};

pub fn server(data: SharedData) -> Result<(), Error> {
    let config = data.clone().config;
    let shared_data = data.clone();

    // These are some `Filter`s that several of the endpoints share,
    // so we'll define them here and reuse them below...

    // Turn our "state", our db, into a Filter so we can combine it
    // easily with others...
    let with_data = warp::any().map(move || shared_data.clone());

    // demo route
    let hello = path!("hello" / String).map(|name| format!("Hello, {}!", name));

    // graphs
    let latest = path!("data" / "latest.json")
        .and(with_data.clone())
        .and_then(get_last_temperature);
    let graph = path!("data" / String / "graph.png")
        .and(with_data.clone())
        .and_then(get_graph);
    let data = latest.or(graph);

    let prom = path!("metrics").and(with_data.clone()).and_then(get_prom);

    // web ui
    let mut webui_path = std::env::current_dir()?;
    webui_path.push("webui");
    let webui = warp::fs::dir(webui_path);

    // compose routes, add logging middleware
    let routes = hello
        .or(data)
        .or(webui)
        .or(prom)
        .with(warp::log("webui"))
        .recover(mask_error);

    // start the server!
    let listen_ip: IpAddr = config.webui.listen_ip.parse()?;
    println!(
        "Starting Warp server on {}:{}",
        config.webui.listen_ip, config.webui.listen_port
    );
    warp::serve(routes).run((listen_ip, config.webui.listen_port));

    Ok(())
}

fn get_graph(timeframe: String, data: SharedData) -> Result<impl Reply, Rejection> {
    let img = data
        .storage
        .lock()
        .graph(&timeframe)
        .map_err(warp::reject::custom)?;

    let response = Response::builder()
        .header(CONTENT_TYPE, HeaderValue::from_static("image/png"))
        .status(200)
        .body(img);

    Ok(response)
}

fn get_prom(data: SharedData) -> Result<impl Reply, Rejection> {
    match get_prom_inner(data) {
        Ok(bytes) => Ok(bytes),
        Err(e) => Err(warp::reject::custom(e)),
    }
}

fn get_prom_inner(data: SharedData) -> Result<Vec<u8>, Error> {
    match data.storage.lock().last_entry() {
        None => Err(err_msg("No entries collected yet!")),
        Some(entry) => {
            let temp_gauge = Gauge::new("temperature", "Measured ambient temperature.")?;
            temp_gauge.set(entry.value);

            let registry = Registry::new();
            registry
                .register(Box::new(temp_gauge.clone()))
                .map_err(|e| err_msg(format!("Error registering with prometheus!, {:?}", e)))?;

            let mut buffer = vec![];
            let encoder = TextEncoder::new();
            let metric_families = registry.gather();
            encoder.encode(&metric_families, &mut buffer).unwrap();

            Ok(buffer)
        }
    }
}

fn get_last_temperature(data: SharedData) -> Result<impl Reply, Rejection> {
    match data.storage.lock().last_entry() {
        None => Err(warp::reject::custom(err_msg("No entries collected yet!"))),
        Some(entry) => Ok(warp::reply::json(entry)),
    }
}

fn mask_error(err: Rejection) -> Result<impl Reply, Rejection> {
    match err.cause() {
        Some(c) => {
            println!("Internal server error: {:?}", c);
            Ok(StatusCode::INTERNAL_SERVER_ERROR)
        }
        None => Err(err),
    }
}
