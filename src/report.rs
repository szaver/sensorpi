use crate::config::Config;
use crate::email::SmtpClient;
use crate::storage::Storage;
use failure::{Error, ResultExt};
use log::debug;
use parking_lot::Mutex;
use std::sync::Arc;

pub fn send_report(
    config: &Config,
    storage: Arc<Mutex<Storage>>,
    email_client: &SmtpClient,
) -> Result<(), Error> {
    debug!("send_report");
    debug!("Generating plot");
    let plot_bytes = storage
        .lock()
        .graph("now-85000s")
        .context("error plotting temperature for report")?;

    let subject = format!("Resumen diaria de {site}", site = config.site_name);

    for address in config.smtp.subscribers() {
        debug!("Building email");
        let mut email_builder = email_client.html_builder(&address, &subject, &subject);
        debug!("Attaching plot to email");
        email_builder = email_builder
            .attachment(&plot_bytes.clone(), "temp-24hr.png", &mime::IMAGE_PNG)
            .context("Error setting attachment")?;

        debug!("Finalizing email builder");
        let email = email_builder
            .build()
            .context("Error building summary email")?;

        debug!("Sending email");
        email_client.send(email).context("Error sending email.")?;
        debug!("Finished sending email");
    }
    debug!("end send_report");
    Ok(())
}
