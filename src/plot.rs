use crate::config::Config;
use crate::reading::Quantity;
use crate::storage::Storage;
use chrono::{DateTime, Utc};
use failure::{Error, ResultExt};
use gnuplot::*;
use parking_lot::Mutex;
use std::sync::Arc;

pub fn recent_plot(
    config: &Config,
    storage: Arc<Mutex<Storage>>,
    quantity: Quantity,
) -> Result<String, Error> {
    let entries: Vec<(DateTime<Utc>, f64)> = storage
        .lock()
        .recent_entries()
        .iter()
        .filter(|reading| reading.quantity == quantity)
        .map(|reading| (reading.timestamp, reading.value))
        .collect();

    let img_path = config.fs.tmp.clone() + "/recent-temperature.png";
    mk_plot(
        &img_path,
        "Temperatura Recente (tiempos en UTC)",
        "Temperatura (˚C)",
        entries,
    );

    Ok(img_path)
}

pub fn aggregate_plot(
    config: &Config,
    storage: Arc<Mutex<Storage>>,
    quantity: Quantity,
) -> Result<String, Error> {
    let entries = storage
        .lock()
        .aggregated_entries()
        .context("loading aggregated entries")?
        .iter()
        .filter(|reading| reading.quantity == quantity)
        .map(|reading| (reading.end_timestamp, reading.mean))
        .collect();

    let img_path = config.fs.tmp.clone() + "/aggregate-temperature.png";
    mk_plot(
        &img_path,
        "Temperatura Historico (tiempos en UTC)",
        "Temperatura (˚C)",
        entries,
    );

    Ok(img_path)
}

pub fn mk_plot(output: &str, title: &str, caption: &str, data: Vec<(DateTime<Utc>, f64)>) {
    let x: Vec<f64> = data.iter().map(|r| r.0.timestamp() as f64).collect();
    let y: Vec<f64> = data.iter().map(|r| r.1).collect();
    let mut fg = Figure::new();
    fg.set_terminal("pngcairo size 800, 600", output);

    fg.axes2d()
        .set_title(title, &[])
        .lines(
            &x,
            &y,
            &[Caption(caption), Color("#173f5f"), LineWidth(2.0)],
        )
        .set_grid_options(true, &[LineStyle(SmallDot), Color("black")])
        .set_x_grid(true)
        .set_y_grid(true)
        .set_x_ticks(Some((Auto, 1)), &[Format("%H:%M")], &[])
        .set_x_time(true)
        .set_x_range(Auto, Auto);
    fg.show();
    fg.close();
}
