use crate::config::Alert as AlertConfig;
use crate::email::SmtpClient;
use crate::reading::{Quantity, Reading};
use failure::Error;
use std::sync::Arc;

pub struct Alert {
    // config
    email_client: Arc<SmtpClient>,
    alert_config: Option<AlertConfig>,
    quantity: Quantity,
    subscribers: Vec<String>,
    site_name: String,

    // state
    is_triggered: bool,
}

impl Alert {
    pub fn new(
        email_client: Arc<SmtpClient>,
        alert_config: &Option<AlertConfig>,
        quantity: Quantity,
        site_name: &str,
        subscribers: Vec<String>,
    ) -> Self {
        Self {
            email_client,
            quantity,
            alert_config: alert_config.clone(),
            is_triggered: false,
            subscribers,
            site_name: site_name.to_string(),
        }
    }
    pub fn process(&mut self, reading: &Reading) -> Result<bool, Error> {
        // Ignore unconfigured alerts
        let alert_config = match &self.alert_config {
            None => return Ok(false),
            Some(a) => a,
        };

        // ignore readings which weren't meant for us
        if reading.quantity != self.quantity {
            return Ok(false);
        }

        // apply rules
        if self.is_triggered {
            // triggered!  Now check if it's time to un-trigger.
            if reading.value < alert_config.clear {
                self.is_triggered = false;
                self.send_ok(reading)?;
            }
        } else if reading.value > alert_config.trigger {
            // not triggered, but it's time to trigger
            self.is_triggered = true;
            self.send_alert(reading)?;
        };

        Ok(self.is_triggered)
    }

    pub fn process_batch(
        &mut self,
        readings: impl IntoIterator<Item = Reading>,
    ) -> Result<(), Error> {
        for reading in readings {
            self.process(&reading)?;
        }

        Ok(())
    }

    fn send_alert(&self, reading: &Reading) -> Result<(), Error> {
        let msg = format!(
            "{name} alta en {site}! ({value} {unit})",
            name = reading.quantity.name_title(),
            value = reading.value,
            unit = reading.quantity.unit(),
            site = self.site_name
        );
        for email in &self.subscribers {
            self.email_client.send_plain(&email, &msg, &msg)?;
        }

        Ok(())
    }

    fn send_ok(&self, reading: &Reading) -> Result<(), Error> {
        let msg = format!(
            "{name} en {site} volvio a normal. ({value} {unit})",
            name = reading.quantity.name_title(),
            value = reading.value,
            unit = reading.quantity.unit(),
            site = self.site_name
        );
        for email in &self.subscribers {
            self.email_client.send_plain(&email, &msg, &msg)?;
        }

        Ok(())
    }
}
