use crate::config::Calibrations;
use crate::reading::{Quantity::*, Reading};
use crate::sensor::Sensor;
use failure::{err_msg, Error, ResultExt};
use rand::prelude::*;
use serde::Deserialize;
use std::env;
use std::process::Command;

pub struct Bme280 {
    name: String,
    read_command: Command,
    calibrations: Calibrations,
}

impl Bme280 {
    pub fn new(
        name: impl Into<String>,
        calibrations: &Calibrations,
        read_command: Command,
    ) -> Self {
        Self {
            name: name.into(),
            read_command,
            calibrations: calibrations.clone(),
        }
    }

    fn read_with_cmd(command: &mut Command) -> Result<Bme280Reading, Error> {
        let output = command.output().context("Couldn't run the read command")?;

        // get process stdout
        let reading = String::from_utf8(output.stdout).context("stdout is invalid utf8")?;

        // parse it (should be in json format)
        let reading: Bme280Reading =
            serde_json::from_str(&reading).context("error parsing reading with serde")?;

        if reading.pressure < 1.0 {
            return Err(err_msg("Data doesn't pass the sniff test!"));
        }

        Ok(reading)
    }
}

impl Sensor for Bme280 {
    fn read(&mut self) -> Result<Vec<Reading>, Error> {
        let bme280_reading = if env::var_os("MOCK_SENSOR").is_none() {
            // read from real sensor
            Self::read_with_cmd(&mut self.read_command).context("reading the bme280 sensor")?
        } else {
            let mut rng = rand::thread_rng();
            // get mock readings
            Bme280Reading {
                sensor: "bme280_fake".into(),
                humidity: rng.gen_range(0.0, 100.0),
                pressure: rng.gen_range(100.0, 1000.0),
                temperature: rng.gen_range(0.0, 100.0),
                altitude: rng.gen_range(0.0, 100.0),
                timestamp: 12_341_234,
            }
        };

        // convert to array of readings and calibrate
        let readings = bme280_reading.to_readings(&self.calibrations, &self.name);

        Ok(readings)
    }
}

#[derive(Deserialize, Debug, Clone)]
struct Bme280Reading {
    sensor: String,
    humidity: f64,
    pressure: f64,
    temperature: f64,
    altitude: f64,
    timestamp: u64,
}

impl Bme280Reading {
    fn to_readings(&self, calibrations: &Calibrations, name: &str) -> Vec<Reading> {
        vec![
            Reading::now(name, Humidity, self.humidity).calibrate(&calibrations.humidity),
            Reading::now(name, Temperature, self.temperature).calibrate(&calibrations.temperature),
            Reading::now(name, Pressure, self.pressure).calibrate(&calibrations.pressure),
        ]
    }
}
