#!/usr/bin/env bash



if [ ! -f '/data/temperature.rrd' ]; then
    echo "No database found.  Creating it now."

    rrdtool create /data/temperature.rrd --step=1 \
        DS:temperature:GAUGE:2:U:U `# one reading every second, timeout after two seconds` \
        RRA:AVERAGE:0.9:1s:15m `# one reading every 1 second for the past 15 minutes` \
        RRA:AVERAGE:0.9:4s:1h `# one reading every 4 seconds for the past hour` \
        RRA:AVERAGE:0.9:85s:85000s `# one reading every 85 seconds for the past 24 hours` \
        RRA:AVERAGE:0.9:600s:1w `# one reading every 600 seconds for the past week` \
        RRA:AVERAGE:0.9:2400s:1M `# one reading every 2400 seconds for the past month` \
        RRA:AVERAGE:0.9:32000s:32000000s `# one reading every 29000 seconds for the past year` \
        RRA:AVERAGE:0.9:320000s:320000000s `# one reading every 290000 seconds for the past 10 years`
fi

./sensorpi

if [ -n "$NO_RESTART" ]; then
    echo “Application exited.  Not restarting because NO_RESTART was set”
    while : ; do
        echo “Idling…”
        sleep 86400
    done
fi