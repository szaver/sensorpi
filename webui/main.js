const app = new Vue({
    el: "#app",
    data: {
        latest_msg: "Cargando datos..."
    }
});

const fetchTemp = () => {
    fetch("/data/latest.json")
        .then(response => {
            return response.json();
        })
        .then(reading => {
            console.log(reading);
            if (
                reading &&
                reading.value &&
                reading.quantity &&
                reading.quantity == "Temperature"
            ) {
                app.latest_msg = `Temperatura: ${reading.value.toFixed(2)} ˚C`;
            } else {
                app.latest_msg = "Error en cargar valor actual.";
            }
        });

    setTimeout(fetchTemp, 1000);
};

fetchTemp();

setInterval(() => document.location.reload(), 60000);
