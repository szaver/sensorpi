################################################################################
# Base image
################################################################################

FROM balenalib/%%RESIN_MACHINE_NAME%%-debian:buster-build as build

ENV DEBIAN_FRONTEND=noninteractive

################################################################################
# Rust image
################################################################################

FROM build as rust-toolchain

ENV PATH=/root/.cargo/bin:$PATH

# Modify `uname -m` for `arm7hf` and `rpi` RESIN_ARCH values:
# https://forums.resin.io/t/rustup-fails-for-armv8l/2661
# -> https://forums.resin.io/t/resin-build-variable-inconsistency/1571/2
# -> https://github.com/resin-io/docs/issues/739
# bump ---
# https://github.com/rust-lang-nursery/rustup.rs/issues/1055
WORKDIR /build
COPY scripts/modify-uname.sh .
RUN ./modify-uname.sh %%RESIN_ARCH%%

# Install rustup downloading the version specified by the standard rust-toolchain file
COPY rust-toolchain .
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain `cat rust-toolchain`

################################################################################
# Dependencies
################################################################################

FROM rust-toolchain as rust-deps

WORKDIR /build

# Create new fake project ($USER is needed by `cargo new`)
RUN USER=root cargo new app

WORKDIR /build/app

# Copy real app dependencies
COPY Cargo.* ./
# Copy the file specifying the Rust version to use
COPY rust-toolchain ./

# Build fake project with real dependencies
RUN cargo build --release

# Remove the fake app build artifacts
RUN rm -rf target/release/sensorpi* target/release/deps/sensorpi-*

################################################################################
# Rust Binary Builder
################################################################################

FROM rust-deps as rust-binary

# We do not want to download deps, update registry, ... again
COPY --from=rust-deps /root/.cargo /root/.cargo

WORKDIR /build/app

# Copy everything, not just source code
COPY ./src ./src

# Update already built deps from dependencies image
COPY --from=rust-deps /build/app/target target

# Build real app
RUN cargo build --release

################################################################################
# BME280 Builder
################################################################################

FROM balenalib/%%RESIN_MACHINE_NAME%%-debian:buster-build as bme280-binary

# dependencies
RUN ["install_packages", "libi2c-dev", "i2c-tools", "wiringpi"]

# Build BME280
RUN touch /bust_cache
RUN git clone https://github.com/whmountains/raspberry-pi-bme280 /bme280
WORKDIR /bme280
RUN make


################################################################################
# Production Base image
################################################################################

FROM balenalib/%%RESIN_MACHINE_NAME%%-debian:buster-run as base

# Add Tini
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-armhf /tini
RUN chmod +x /tini

# shared libraries
RUN ["install_packages", "libi2c-dev", "i2c-tools", "wiringpi", "rrdtool", "nmap", "iputils-arping", "iputils-ping", "snmp"]

################################################################################
# Final image
################################################################################

FROM base

WORKDIR /app

# Copy binary from builder image
COPY --from=rust-binary /build/app/target/release/sensorpi .

# Copy BME280 builder image
COPY --from=bme280-binary /bme280/bme280-77 /bme280-77
COPY --from=bme280-binary /bme280/bme280-76 /bme280-76

# Copy configuration defaults
COPY default_settings.toml .

# Copy run script
COPY run.sh .

# Copy other folders required by the application. Example:
COPY webui ./webui

# Launch application
ENTRYPOINT ["/tini", "--"]
CMD ["./run.sh"]
