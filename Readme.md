# Sensorpi

Sensorpi can:
* Read temperature, humidity, and pressure from a BME280 sensor.
* Display the results via a web ui.
* Send daily summary emails
* Send alert emails

It is designed to be deployed on a Raspberry Pi with [Balena](https://www.balena.io/).

## Getting Started

Sensorpi is configured via environment variables.  Check out src/config.rs to get an idea of what configuration it needs.  You can access nested configuration variables with a double underscore, so `smtp.server_domain` could be specified as `smtp__server_domain`.  It's kind of ugly, but it works.

## Contributing

Pull requests are welcome!  If your PR is merged I will probably add you as a maintainer.